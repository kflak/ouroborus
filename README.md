# Ouroboros

Music for the dance performances Feeding Ouroboros and Matter Matters by
[Roosna & Flak](https://roosnaflak.com)

## Installation

Evaluate the following line of code in SuperCollider:
`Quarks.install("https://gitlab.com/kflak/ouroboros")` This will,
however, not work out of the box without the locally stored audio files
(symlinked in the audio directory). Also needs a working setup of
[MiniBee sensors](https://sensestage.eu/minibee/).

## Dependencies
[KF Quark](https://gitlab.com/kflak/kf)  
[MiniBee Quark](https://gitlab.com/kflak/minibee)  
[CuePlayer Quark](https://github.com/dathinaios/CuePlayer)

## Motor setup

1. Start the ResponsiveBody network
2. Connect the laptop to the TP-link router via ethernet
3. Evaluate the first block of `init.scd`
4. Restart the motors. When they no longer flash the blue led they
   should be connected.
