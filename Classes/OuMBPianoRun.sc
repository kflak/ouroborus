OuMBPianoRun : OuMBDeltaTrig {

    *new{
        arg db=0, speedlim=0.5, threshold=0.05, minAmp= -10, maxAmp=6, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBPianoRun;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, Prand(KF.buf[\pianoRun]),
                \dur, dt.linlin(0.0, 1.0, 0.1, 2),
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 4,
                \rate, Prand([2, 4]),
                \posRate, Pwhite(0.2, 0.8),
                \startPos, Pfunc{|ev| ev.buf.numFrames.rand },
                \tRateModFreq, 1,
                \tRateModDepth, 1,
                // \rateModFreq, 1,
                // \rateModDepth, 1,
                \posRate, 1,
                \posRateModFreq, 1,
                \posRateModDepth, 1,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp);
            )
        };
    }

    initOuMBPianoRun {
        "Init MBPianoRun".postln;
        fx.filter(1, {|in|
            var verb, mix, sig, gainMod;
            mix = SinOsc.kr(0.2).range(0.1, 0.3);
            gainMod = SinOsc.kr(0.1).range(1, 10);
            sig = in;
            sig = (sig * gainMod).tanh;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix) * 6.dbamp;
            sig = BHiPass.ar(sig, 200);
        });
    }
}
