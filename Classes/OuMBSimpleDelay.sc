OuMBSimpleDelay : OuMBDeltaTrig {

    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -10, maxAmp=6, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBSimpleDelay;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var dur = 1;
            fx[0] = {
                var sig, env, delay, decayTime, delayTime, delayMix = 0.5;
                var gain = \gain.kr(20.dbamp);
                env = Env([0, 1, 0, 0.5, 0, 0.3, 0], [0] ++ (0.01!5)).kr();
                sig = WhiteNoise.ar * env * gain;
                sig = sig.tanh;
                decayTime = dt.linlin(0.0, 1.0, 3, 7);
                delayTime = dt.linlin(0.0, 1.0, 0.3, 0.01);
                sig = MoogVCF.ar(sig, 
                    SinOsc.kr(20).exprange(900, 10000), // cutoff freq
                    LFNoise2.kr(5).range(0.0, 0.7) // q
                );
                sig = MoogVCF.ar(
                    sig,
                    LFNoise2.kr(2).exprange(500, 10000), // cutoff freq
                    LFNoise2.kr(5).range(0.0, 0.7) // q
                );
                delay = CombC.ar(sig, 0.2, delayTime, decayTime);
                sig = (1 - delayMix) * sig + (delay * delayMix);
                sig = sig * LFNoise2.kr(10);
                sig = BHiPass.ar(sig, 80);
                sig = PanAz.ar(KF.numSpeakers, sig, rrand(-1.0, 1.0));
                sig;
            }
        };
    }

    initOuMBSimpleDelay {
        fx.filter(1, {|in|
            var verb, mix, sig;
            // mix = SinOsc.kr(0.2).range(0.05, 0.3);
            sig = in;
            // verb = JPverbMono.ar(in, size: 1, t60: 2);
            // sig = (1 - mix) * sig + (verb * mix);
            sig = BHiPass.ar(sig, 200);
        });
    }
}
