OuMicSmash {
    var <>in;
    var <db;
    var <>fadeTime;
    var <threshold;
    var <>cuePlayer;
    var <>triggerCue = false;
    var nodeProxy;
    var oscFunc;
    var posRate;
    var posRateModDepth;
    var <>celloLevel = -10;
    var <>talkLevel = -4;
    var talkBuffer;
    var talkBufferIndex = 0;

    *new{|in=4, db=0, fadeTime=0.5, threshold= -20|
        ^super.newCopyArgs(in, db, fadeTime, threshold).init;
    }

    // TODO: Split mic and cello triggers into separate NodeProxys,
    // in order to do separate processing on the cellos. Possibly 
    // evolving into ring-modulation?

    db_{|val| nodeProxy.vol = val.dbamp; db = val}
    threshold_{|val| threshold = val; nodeProxy.threshold = val.dbamp}

    init {
        var timeToNormalText = 60;
        talkBuffer = KF.buf[\OuTalks].scramble;
        nodeProxy = NodeProxy.new.play(vol: db.dbamp);
        nodeProxy.fadeTime = fadeTime;
        // celloLevel = Env.new([0, -10], 240).asStream;
        posRate = Env.new([0.1, 1.0], timeToNormalText).asStream;
        // posRateModDepth = Env.new([1.0, 0.0], timeToNormalText).asStream;
        nodeProxy[0] = {
            var sig = SoundIn.ar(in);
            var comb, combMix=0.4, verb, verbMix=0.3;
            var amplitude, trig, timer, filteredTrig;
            sig = sig * \gain.kr(10.dbamp);
            sig = sig.tanh;
            amplitude = Amplitude.kr(sig);
            trig = amplitude > \threshold.kr(threshold.dbamp);
            timer = Timer.kr(trig);
            filteredTrig = (timer > \speedlim.kr(0.5)) * trig;
            SendTrig.kr(filteredTrig, 0, amplitude);
            comb = CombC.ar(sig, maxdelaytime: 0.4, delaytime: 0.5, decaytime: 3);
            sig = (1 - combMix) * sig + (comb * combMix);
            verb = JPverb.ar(sig, size: 3, t60: 4);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = sig * \amp.kr(-6.dbamp);
        };

        oscFunc = OSCFunc.new({|msg|
            var amp = msg[3];
            if(triggerCue && cuePlayer.notNil){
                cuePlayer.next;
                triggerCue = false;
            };
            nodeProxy[1] = Pbind(
                \instrument, \granulator,
                \buf, Prand(KF.buf[\Cello]),
                \dur, Pfunc{|ev| min(4, ev.buf.duration)},
                \attack, 0.01,
                \release, Pkey(\dur),
                \rate, 1,
                \tRateModFreq, 1,
                \tRateModDepth, 5,
                \posRate, Pwhite(0.5, 1.0),
                \posRateModFreq, Pwhite(0.2, 2.0),
                \posRateModDepth, Pwhite(1, 5),
                \pan, Pwhite(-1.0, 1.0),
                \db, celloLevel,
                // \db, amp.linlin(0.0, 1.0, 0, 6) + celloLevel.next,
            );
            nodeProxy[2] = Pbind(
                \instrument, \granulator,
                \buf, talkBuffer[talkBufferIndex],
                \dur, Plazy({|ev|
                    var d = ev.buf.duration;
                    Pseq([Rest(1), d])
                }),
                \attack, 0.01,
                \release, 0.01,
                \legato, 1,
                \rate, 1,
                \posRate, Pfunc{ posRate.next },
                \pan, Pwhite(-1.0, 1.0),
                \db, talkLevel,
            );
            talkBufferIndex = (talkBufferIndex + 1) % talkBuffer.size;
        }, '/tr', KF.server.addr);
    }

    free {|releaseTime|
        if(releaseTime.notNil){
            fadeTime = releaseTime;
        };
        nodeProxy.release(fadeTime);
        oscFunc.clear;
    }
}
