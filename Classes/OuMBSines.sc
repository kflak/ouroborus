OuMBSines : KFMBDeltaTrig {
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -30, maxAmp= 0, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBSines;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \sine,
                \scale, Scale.minor,
                \degree, Pwhite(0, 8, 1),
                \octave, Prand((6..8)),
                \attack, dt.linlin(0.0, 1.0, 1, 0.5),
                \release, dt.linlin(0.0, 1.0, 1, 3),
                \dur, Pkey(\attack),
                \legato, 1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
        };
    }

    initOuMBSines {
        fx.filter(1, {|in|
            var verb, mix=0.8, sig;
            sig = in;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix);
        })
    }
}
