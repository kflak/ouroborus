OuMBSuctions : OuMBDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.04, minAmp= -20, maxAmp=0, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBSuctions;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, Prand(KF.buf[\suctions]),
                \dur, Pfunc{|ev| ev.buf.duration},
                \attack, 0,
                \release, 1,
                \legato, 0.1,
                \rate, 1,
                \loop, 0,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    initMBSuctions{
        fx.filter(1, {|in|
            var verb, revMix, sig;
            var comb, combMix=0.1;
            sig = in;
            // comb = CombC.ar(sig, delaytime: LFNoise2.kr(1).range(30, 33).reciprocal, decaytime: 2);
            // combMix = SinOsc.kr(0.1).range(0.05, 0.7);
            // sig = (1 - combMix) * sig + (comb * combMix);
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            revMix = SinOsc.kr(0.2).range(0.05, 0.2);
            sig = (1 - revMix) * sig + (verb * revMix);
        });
    }
}
