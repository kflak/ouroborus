OuMBCrackle : OuMBDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.04, minAmp= -5, maxAmp=0, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBCrackle;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \crackle,
                \attack, 0,
                \release, 1,
                \freq, 200,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
            );
        }
    }

    initMBCrackle{
        "Init MBCrackle".postln;
        fx.filter(1, {|in|
            var verb, revMix, sig;
            var delay, delayMix=0.1;
            sig = in * 40.dbamp;
            sig = sig.tanh;
            // TODO: filter to mb.x
            sig = BLowCut.ar(sig, 900);
            delay = CombC.ar(sig, 0.3, delaytime: LFNoise2.kr(1).range(0.1, 0.3), decaytime: 3);
            sig = (1 - delayMix) * sig + (delay * delayMix);
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            revMix = SinOsc.kr(0.2).range(0.1, 0.4);
            sig = (1 - revMix) * sig + (verb * revMix);
        });
    }
}
