OuMBBodyPerc : OuMBDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.04, minAmp= -20, maxAmp=0, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBBodyPerc;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, Prand(KF.buf[\bodyPerc]),
                \dur, Pfunc{|ev| ev.buf.duration},
                \loop, 0,
                \attack, 0,
                \release, 1,
                \legato, 0.1,
                \rate, 1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    initMBBodyPerc{
        "Init MBBodyPerc".postln;
        fx.filter(1, {|in|
            var verb, revMix, sig;
            sig = in;
            verb = JPverbMono.ar(in, size: 1, t60: 1);
            revMix = SinOsc.kr(0.2).range(0.01, 0.2);
            sig = (1 - revMix) * sig + (verb * revMix);
            sig = sig * 20.dbamp;
        });
    }
}
