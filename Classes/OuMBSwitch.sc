OuMBSwitch {
    classvar <>activeMBs;

    *add{
        arg mbID = 9, mbDeltaTrig;
        var other;

        if(activeMBs.isNil){
            activeMBs = TwoWayIdentityDictionary.new;
        };

        if(activeMBs[mbID].notNil){
            OuMBSwitch.freeMB(mbID);
        };

        // Has the mbDeltaTrig already been added?
        other = activeMBs.getID(mbDeltaTrig);

        // has the mb been freed previously? if so, add it back in again.
        // if(activeMBs[mbID].isNil){
        //     activeMBs.add(mbID, mbDeltaTrig);
        // };
        
        if(other.notNil){
            // if so, set the mb to this
            activeMBs[mbID] = activeMBs[other];
        }{
            // otherwise, create a new instance.
            activeMBs[mbID] = mbDeltaTrig;
        };
    }

    *play {
        arg mbID;
        if(activeMBs[mbID].isNil){
            "mbID not assigned".warn;
        }{
            activeMBs[mbID].play(mbID);
        }
    }

    *freeMB {
        arg mbID;
        if(activeMBs[mbID].isNil){
            "MB not active".warn;
        }{
            activeMBs[mbID].freeMB(mbID);
            // activeMBs.removeAt(mbID);
        }
    }

    *clear {
        activeMBs.do{
            arg mbID;
            mbID.free;
        };
        activeMBs = nil;
    }
}
