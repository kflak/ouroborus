OuMBXyloStretch : OuMBDeltaTrig {
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -16, maxAmp=0, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBStretchXylo;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, KF.buf[\xylophoneStretch].choose,
                \dur, Pfunc{|ev| ev.buf.duration / 4},
                \attack, 0.01,
                \release, Pkey(\dur),
                \rate, Prand([1, 2]),
                \posRate, Pwhite(0.2, 0.8),
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            )
        };
    }

    initOuMBStretchXylo {
        "Init OuMBXyloStretch".postln;
        fx[1] = Pbind(
            \instrument, \playbuf,
            \buf, KF.buf[\xyloNoise][0],
            \dur, Pfunc{|ev| ev.buf.duration / 2},
            \attack, Pkey(\dur) / 2,
            \release, Pkey(\dur) / 2,
            \legato, 1,
        );
        fx.filter(2, {|in|
            var verb, mix, sig;
            mix = SinOsc.kr(0.2).range(0.3, 0.9);
            sig = in;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix);
            // sig = sig * \amp.kr(1, fadeTime);
        });
    }
}
