OuMBKick : KFMBDeltaTrig {
    var <revMix = 0.5;
    var <gain = 0;

    *new{|db=0, speedlim=0.5, threshold=0.04, minAmp= -20, maxAmp=0, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBKick;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, Prand(KF.buf[\bd]),
                \dur, Pfunc{|ev| ev.buf.duration},
                \attack, 0,
                \release, 0.1,
                \loop, 0,
                \legato, 1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, 0,
            );
        }
    }

    initMBKick{
        fx.filter(1, {|in|
            var verb, revMix = \revMix.kr(revMix), sig;
            sig = in;
            sig = sig * \gain.kr(0.dbamp);
            sig = sig.tanh;
            sig = sig * gain.dbamp.neg;
            verb = JPverbMono.ar(in, size: 2, t60: 3);
            sig = (1 - revMix) * sig + (verb * revMix);
        });
    }
    
    revMix_{|val| revMix = val; fx.set(\revMix, val);}
    gain_{|val| gain = val; fx.set(\gain, val.dbamp);}
}
