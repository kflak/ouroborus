OuMotor {
    classvar <all;
    var <ip;
    var <name;
    // var <setup;
    var <speed;
    var <position;
    var <status;
    var prGetSpeedFunc;
    var prGetPosFunc;
    var prGetStatusFunc;

    *initClass {
        all = IdentityDictionary.new;
    }

    *new{|ip, name|
        ^super.newCopyArgs(ip, name).init;
    }

    init { 
        var netAddr = NetAddr(ip, 12030);

        this.class.all.put(name, netAddr);
        this.pong;

        prGetSpeedFunc = OSCFunc.newMatching({|msg|
            var k = msg[1];
            speed = k;
        }, \ouro_speed, ip);

        prGetPosFunc = OSCFunc.newMatching({|msg|
            var k = msg[1];
            position = k;
        }, \ouro_pos, ip);

        prGetStatusFunc = OSCFunc.newMatching({|msg|
            var k = msg[1].asSymbol;
            status = k;
        }, \ouro_status, ip);
    }

    getPosition{ OuMotor.all[name].sendMsg(\ouro_get_position); }
    getSpeed{ OuMotor.all[name].sendMsg(\ouro_get_speed); }
    getStatus{ OuMotor.all[name].sendMsg(\ouro_status); }

    neo{|...i| OuMotor.all[name].sendMsg(\ouro_neo, i); }
    neoAll{|...i| OuMotor.all[name].sendMsg(\ouro_neo_all, i); }
    neoFade{|i| OuMotor.all[name].sendMsg(\ouro_neo_fade, i); }
    move{|i| OuMotor.all[name].sendMsg(\ouro_move, i); }
    moveTo{|i| OuMotor.all[name].sendMsg(\ouro_move_to, i); }
    forward{|i| OuMotor.all[name].sendMsg(\ouro_forward); }
    backward{|i| OuMotor.all[name].sendMsg(\ouro_backward); }
    setAcceleration{|i| OuMotor.all[name].sendMsg(\ouro_set_acceleration, i); }
    setSpeed{|i| OuMotor.all[name].sendMsg(\ouro_set_speed, i); }
    setPosition{|i| OuMotor.all[name].sendMsg(\ouro_set_position, i); }
    sendArr{|i| OuMotor.all[name].sendMsg(\ouro_send_arr, i); }
    sendEnc{|i| OuMotor.all[name].sendMsg(\ouro_send_enc, i); }
    led{|i| OuMotor.all[name].sendMsg(\ouro_led, i); }
    keepRunning{ OuMotor.all[name].sendMsg(\ouro_keep_running);}
    stop{ OuMotor.all[name].sendMsg(\ouro_stop); }
    forceStop{ OuMotor.all[name].sendMsg(\ouro_force_stop); }
    panic{ OuMotor.all[name].sendMsg(\ouro_panic); }
    reset{ OuMotor.all[name].sendMsg(\ouro_reset); }
    pong{ OuMotor.all[name].sendMsg(\ouro_pong); }

    free {
        // setup.free;
        prGetSpeedFunc.free;
        prGetPosFunc.free;
        prGetStatusFunc.free;
        OuMotor.all[name] = nil;
    }
}
