OuMBStrongExhales : OuMBDeltaTrig {
    var <revMix = 0.2;

    *new{|db=0, speedlim=0.5, threshold=0.04, minAmp= -20, maxAmp=10, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBStrongExhales;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, Prand(KF.buf[\strongExhales]),
                \dur, Pfunc{|ev| ev.buf.duration},
                \attack, 0,
                \release, 0.1,
                \loop, 0,
                \legato, 1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, 0,
            );
        }
    }

    initMBStrongExhales{
        fx.filter(1, {|in|
            var verb, revMix = \revMix.kr(revMix), sig;
            sig = in;
            verb = JPverbMono.ar(in, size: 2, t60: 3);
            sig = (1 - revMix) * sig + (verb * revMix);
        });
    }
    
    revMix_{|val| revMix = val; fx.set(\revMix, val);}
}
