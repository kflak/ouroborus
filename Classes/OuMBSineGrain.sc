OuMBSineGrain : OuMBDeltaTrig {
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -30, maxAmp= 0, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBSineGrain;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \sineGrain,
                \freq, Pexprand(660, 1100),
                \tFreq, Pexprand(0.5, 2.0),
                // \tFreq, Pexprand(1.0, 10.0),
                \grainSize, Pexprand(0.0001, 0.1),
                \attack, dt.linlin(0.0, 1.0, 1, 0.5),
                \release, dt.linlin(0.0, 1.0, 1, 3),
                \dur, Pkey(\attack),
                \legato, 1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1);
            );
        };
    }

    // \combdelay, rrand(0.0001, 0.001),
    // \combdecay, rrand(0.1, 2.0),
    // \comblvl, rrand(0.7, 1.0),
    // \hipassfreq, 150,
    // \hishelffreq, 1000,
    // \hishelfdb, -12,

    initOuMBSineGrain {
        fx.filter(1, {|in|
            var verb, revMix=0.8, comb, combMix, sig;
            sig = in;
            comb = CombC.ar(sig, delaytime: LFNoise2.kr(1).range(30, 33).reciprocal, decaytime: 2);
            combMix = SinOsc.kr(0.1).range(0.01, 0.7);
            sig = (1 - combMix) * sig + (comb * combMix);
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - revMix) * sig + (verb * revMix);
        })
    }
}
