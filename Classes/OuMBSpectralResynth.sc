OuMBSpectralResynth : OuMBDeltaTrig {
    var <>baseFreqs = #[200, 233, 700, 957, 1190, 1423, 1652, 1889, 2119, 2362, 2835];
    var <>dbs = #[ -20, -29, -30, -32, -32, -37, -37, -44, -52, -49, -60 ];
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -10, maxAmp=6, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBSpectralResynth;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \resynth,
                \scale, Scale.minor,
                \degree, Pwhite(0, 8, 1),
                \octave, Pwrand((2..5), [0.5, 0.4, 0.1].normalizeSum, inf),
                \attack, dt.linlin(0.0, 1.0, 2, 1),
                \release, dt.linlin(0.0, 1.0, 3, 15),
                \dur, Pkey(\attack),
                \baseFreqs, [baseFreqs],
                \dbs, [dbs],
                \legato, 1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp);
            );
        };
    }

    initOuMBSpectralResynth {
        fx.filter(1, {|in|
            var verb, mix=0.8, sig;
            sig = in;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix);
        })
    }
}
