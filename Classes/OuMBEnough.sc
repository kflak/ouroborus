OuMBEnough : OuMBDeltaTrig {
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.05, minAmp= -30, maxAmp=0, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBEnough;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, Prand(KF.buf[\moondogEnough]),
                \dur, dt.linlin(0.0, 1.0, 0.1, 2),
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 4,
                \rate, Prand([0.25, 0.5, 1]),
                \posRate, Pwhite(0.2, 0.8),
                \startPos, Pfunc{|ev| ev.buf.numFrames.rand },
                \tRateModFreq, 1,
                \tRateModDepth, 1,
                // \rateModFreq, 1,
                // \rateModDepth, 1,
                \posRate, 1,
                \posRateModFreq, 1,
                \posRateModDepth, 1,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp);
            )
        };
    }

    initOuMBEnough {
        fx.filter(1, {|in|
            var verb, mix, sig, gainMod;
            mix = SinOsc.kr(0.2).range(0.3, 0.9);
            gainMod = SinOsc.kr(0.1).range(1, 10);
            sig = in;
            sig = (sig * gainMod).tanh;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix);
        });
    }
}
