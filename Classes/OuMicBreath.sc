OuMicBreath {
    var <in;
    var <db;
    var <>fadeTime;
    var <pan;
    var fx;
    var <gain = 10;
    var <modFreqModFreq;
    var <modFreqModDepth = 2;
    var <modFreq = 5;
    var <modDepth;
    var <verbMix = 0.1;
    var <combMix = 0.2;
    var <delayTime = 0.01;
    var <decayTime = 3;
    var <dist = 0;
    var <noiseMix = 0;

    *new{|in = 0, db = 0, fadeTime = 1, pan = 0|
        ^super.newCopyArgs(in, db, fadeTime, pan).init;
    }

    dist_{|val| dist = val; fx.set(\dist)}
    db_{|val| db = val; fx.set(\amp, db.dbamp)}
    gain_{|val| gain = val; fx.set(\gain, gain)}
    pan_{|val| pan = val; fx.set(\pan, pan)}
    modFreqModFreq_{|val| modFreqModFreq = val; fx.set(\modFreqModFreq, modFreqModFreq)}
    modFreqModDepth_{|val| modFreqModDepth = val; fx.set(\modFreqModDepth, modFreqModDepth)}
    modFreq_{|val| modFreq = val; fx.set(\modFreq, modFreq)}
    modDepth_{|val| modDepth = val; fx.set(\modDepth, modDepth)}
    verbMix_{|val| verbMix = val; fx.set(\verbMix, verbMix)}
    combMix_{|val| combMix = val; fx.set(\combMix, combMix)}
    delayTime_{|val| delayTime = val; fx.set(\delayTime, delayTime)}
    decayTime_{|val| decayTime = val; fx.set(\decayTime, decayTime)}
    noiseMix_{|val| noiseMix = val; fx.set(\noiseMix, noiseMix)}

    init {
        fx = NodeProxy.audio(KF.server, KF.numSpeakers);
        fx.play(fadeTime: fadeTime, vol: db.dbamp);
        fx.source = {
            var sig = SoundIn.ar(in);
            var verb, comb;
            var mod, modF, modFreqMod, lag = 1;
            var noise, noiseMix = \noiseMix.kr(0);
            modFreqModFreq = \modFreqModFreq.kr(0.5, lag);
            modFreqMod = SinOsc.kr(modFreqModFreq).range(1, \modFreqModDepth.kr(1, lag));
            modDepth = \modDepth.kr(5, lag);
            modFreq = \modFreq.kr(modFreq, lag);
            mod = SinOsc.kr(modFreq * modFreqMod).range(1, modDepth);
            sig = sig * \gain.kr(gain).dbamp;
            sig = sig.tanh * mod;
            // sig = sig * (gain * -1 / 2);
            verb = JPverbMono.ar(sig, size: 3, t60: 4);
            verbMix = \verbMix.kr(verbMix);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = BHiPass.ar(sig, 80);
            delayTime = \delayTime.kr(delayTime, lag);
            decayTime = \decayTime.kr(decayTime, lag);
            comb = CombC.ar(sig, delaytime: delayTime, decaytime: decayTime);
            combMix = \combMix.kr(combMix, lag);
            sig = (1 - combMix) * sig + (comb * combMix);
            sig = FreqShift.ar(sig, 7);
            // feedback destructor?
            sig = DelayC.ar(sig, 0.01, LFNoise2.kr(2).range(0.0001, 0.001));
            sig = BHiPass.ar(sig, 80);
            noise = sig * LFNoise2.kr(\noiseFreq.kr(100));
            sig = (1 - noiseMix) * sig + (noise * noiseMix);
            sig = sig * \dist.kr(dist.dbamp);
            sig = sig.tanh;
            sig = sig * \amp.kr(db.dbamp);
            sig = PanAz.ar(KF.numSpeakers, sig, \pan.kr(pan));
        }
    }

    free {|releaseTime|
        if(releaseTime.isNil){
            fx.free(fadeTime);
        }{
            fx.free(releaseTime);
        }
    }
}
