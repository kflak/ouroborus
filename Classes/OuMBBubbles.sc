OuMBBubbles : OuMBDeltaTrig {
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -10, maxAmp=6, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBBubbles;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var dur = 1;
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, KF.buf[\blowBubbles][0],
                \dur, dt.linlin(0.0, 1.0, 0.1, 1),
                \startPos, Pfunc{|ev| ev.buf.numFrames.rand },
                \attack, Pkey(\dur) / 2,
                \release, Pkey(\dur) * 4,
                \rate, 1,
                // \tRate, Pwhite(5, 20),
                \posRate, 1,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
            fx[1] = Pbind(
                \instrument, \granulator,
                \buf, KF.buf[\blowBubbles][1],
                \dur, dt.linlin(0.0, 1.0, 0.1, 1),
                // \dur, Pfunc{ dur * 3 },
                \attack, Pkey(\dur) / 2,
                \release, Pkey(\dur) * 4,
                \rate, 1,
                \posRate, 1,
                \startPos, Pfunc{|ev| ev.buf.numFrames.rand },
                // \tRate, Pwhite(5, 20),
                \tRateModFreq, 1,
                \tRateModDepth, 1,
                // \rateModFreq, 1,
                // \rateModDepth, 1,
                // \posRate, 1,
                // \posRateModFreq, 1,
                // \posRateModDepth, 1,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            )
        };
    }

    initOuMBBubbles {
        "Init OuMBBubbles".postln;
        fx.filter(2, {|in|
            var verb, mix, sig;
            mix = SinOsc.kr(0.2).range(0.3, 0.9);
            sig = in;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix) * 12.dbamp;
            sig = BHiPass.ar(sig, 80);
        });
    }
}
