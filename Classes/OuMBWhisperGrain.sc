OuMBWhisperGrain : OuMBDeltaTrig {

    *new {
        arg db=0, speedlim=0.5, threshold=0.01, minAmp= -30, maxAmp=0, fadeTime=30;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBWhisperGrain;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var startPos, currentFx, buf, numFrames,
            grainsize, numGrains, step, len, pos, rate, duration,
            attack, release, legato;
            buf = KF.buf[\coronaWhisper].choose;
            numFrames = buf.numFrames;
            grainsize = 0.2;
            numGrains = 20;
            step = grainsize * KF.server.sampleRate;
            ////in samples, not seconds....
            len = dt.linlin(0.0, 1.0, step, step * numGrains);
            startPos = 0;
            pos = (startPos, (startPos+step)..(startPos+len));
            pos = pos.select({|i| i < numFrames});
            rate = 1;
            attack = 0.2;
            release = 0.5;

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, grainsize,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 2,
                \startPos, Pseq(pos),
                \legato, 1,
                \rate, Pwhite(0.99, 1.01) * -1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    initMBWhisperGrain {
        fx.filter(1, {|in|
            var comb, combMix = 0.4;
            var verb, verbMix = 0.15;
            var sig = in;
            sig = Compander.ar(sig, sig, -12.dbamp, slopeAbove: 1/4);
            sig = sig * 10.dbamp;
            comb = CombN.ar(sig, 0.2, 0.2, 4, 1/3)
            + CombN.ar(sig, 0.5, 0.5, 4, 1/3)
            + CombN.ar(sig, 0.7, 0.7, 4, 1/3);
            sig = (1 - combMix) * sig + (comb * combMix);
            sig = BHiPass.ar(sig, 300);
            // sig = BHiShelf.ar(sig, 400, db: -6);
            verb = JPverbMono.ar(sig, size: 1, t60: 1);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = sig * 23.dbamp;
        })
    }
}
