OuMBCeramics : OuMBDeltaTrig {

    *new{
        arg db=0, speedlim=0.5, threshold=0.05, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBCeramics;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, Prand(KF.buf[\ceramics]),
                \dur, Pfunc{|ev| min(2, ev.buf.duration)},
                \attack, 0.01,
                \release, Pkey(\dur) / 2,
                \rate, Prand([0.5, 1, 2]),
                \tRateModFreq, Pwhite(10, 100),
                \tRateModDepth, 0.1,
                \rateModFreq, Pwhite(10, 100),
                \rateModDepth, 0.01,
                \posRate, Pexprand(0.1, 2),
                \posRateModFreq, Pwhite(10, 100),
                \posRateModDepth, 1,
                \pan, Pwhite(-1.0, 1.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
        }
    }

    initMBCeramics {
        "Init MBCeramics".postln;
        fx.filter(1, {|in|
            var verb, mix, sig;
            mix = SinOsc.kr(0.2).range(0.1, 0.3);
            sig = in;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix);
        });
    }
}
