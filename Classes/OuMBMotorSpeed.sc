OuMBMotorSpeed {
    var <motor;
    var <>minSpeed;
    var <>maxSpeed;
    var <mb;
    var task;

    *new{|motor, minSpeed=10000, maxSpeed=700, mb=9|
        ^super.newCopyArgs(motor, minSpeed, maxSpeed, mb).init;
    }

    init {
        task = TaskProxy.new({
            inf.do{
                var speed = KF.mbData[mb].x.linlin(0.0, 1.0, minSpeed, maxSpeed);
                // motor.setSpeed(speed);
                // motor.backward;
                motor.sendMsg(\ouro_set_speed, speed);
                0.05.wait;
                motor.sendMsg(\ouro_backward);
                0.05.wait;
            }
        }).play;
    }

    free {
        task.clear;
    }
}
