OuMBSand : OuMBDeltaTrig {
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -10, maxAmp=6, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBSand;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var dur = 1;
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, KF.buf[\sand].choose,
                \dur, Pfunc{|ev| var d = ev.buf.duration / 2; dur = d; d},
                \attack, Pkey(\dur) / 2,
                \release, Pkey(\dur) / 2,
                // \release, Pkey(\dur) * 4,
                \rate, 0.5,
                \posRate, 0.5,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
            fx[1] = Pbind(
                \instrument, \granulator,
                \buf, KF.buf[\beachWaveLoop][0],
                \dur, Pfunc{ dur * 3 },
                \attack, Pkey(\dur) / 2,
                \release, Pkey(\dur) * 4,
                \rate, 0.5,
                \posRate, 0.5,
                \startPos, Pfunc{|ev| ev.buf.numFrames.rand },
                \tRateModFreq, 1,
                \tRateModDepth, 1,
                // \rateModFreq, 1,
                // \rateModDepth, 1,
                // \posRate, 1,
                // \posRateModFreq, 1,
                // \posRateModDepth, 1,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            )
        };
    }

    initOuMBSand {
        fx.filter(2, {|in|
            var verb, mix, sig;
            mix = SinOsc.kr(0.2).range(0.3, 0.9);
            sig = in;
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix) * 6.dbamp;
        });
    }
}
