OuMBWaterSplash : OuMBDeltaTrig {
    
    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -10, maxAmp=6, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBWaterSplash;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var dur = 1;
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, KF.buf[\WaterSplash].choose,
                \dur, Pfunc{|ev| var d = ev.buf.duration / 2; dur = d; d},
                \attack, Pkey(\dur) / 2,
                \release, Pkey(\dur) / 2,
                // \release, Pkey(\dur) * 4,
                \rate, 1,
                \posRate, 1,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
        };
    }

    initOuMBWaterSplash {
        fx.filter(2, {|in|
            var verb, mix, sig;
            mix = SinOsc.kr(0.2).range(0.3, 0.9);
            sig = in;
            verb = JPverb.ar(in, size: 1, t60: 2);
            sig = (1 - mix) * sig + (verb * mix) * 6.dbamp;
        });
    }
}
