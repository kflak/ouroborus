OuMBAngelDust : OuMBDeltaTrig {

    var currentPos = 0;
    var initTime;
    var elapsedTime;
    var mutateMags;
    var <mutate = false;

    *new{|db=0, speedlim=0.5, threshold=0.01, minAmp= -10, maxAmp=6, fadeTime=30|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBAngelDust;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            var buf = KF.buf[\verkstedshallen].choose;
            var numFrames = buf.numFrames;
            var dur = 0.2;
            var step = dur * KF.server.sampleRate;
            var len = dt.linlin(0.0, 1.0, step, step * 10);
            var pos = (currentPos, currentPos+step..currentPos+len);
            currentPos = pos[pos.size-1].mod(numFrames);
            if(mutate.not){
                this.prSetMags( dt.linlin(0, 1, 256, 0) );
            };

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, dur,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 4,
                \startPos, Pseq(pos),
                \legato, 2,
                \rate, Pwhite(0.25, 1.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    initMBAngelDust{
        "Init MBAngelDust".postln;
        initTime = thisThread.seconds;
        elapsedTime = initTime;
        this.prCreateFx;
        freeMBsImmediately = false;
        fx.filter(1, {|in|
            var mix, lag, numFrames, magnitudes, chain, tobin, sig;
            numFrames = 2048;
            tobin = 256;
            lag = \lag.kr(0.1);
            mix = \mix.kr(1, lag);
            // in = In.ar(\in.kr((KF.numSpeakers + KF.numSubs), KF.numSpeakers)); 
            chain = FFT(LocalBuf(numFrames ! KF.numSpeakers), in);
            magnitudes = \magnitudes.kr(1 ! tobin, lag);
            chain = chain.collect{|i|
                i.pvcalc(numFrames, {|mags|
                    mags * magnitudes;
                }, tobin: tobin);
            };
            sig = IFFT(chain);
        });
        fx.filter(2, {|in|
            var verb, revMix=0.3, sig;
            var comb, combMix=0.3;
            sig = in;
            comb = CombC.ar(sig, delaytime: LFNoise2.kr(1).range(30, 33).reciprocal, decaytime: 2);
            combMix = SinOsc.kr(0.1).range(0.01, 0.7);
            sig = (1 - combMix) * sig + (comb * combMix);
            verb = JPverbMono.ar(in, size: 1, t60: 2);
            sig = (1 - revMix) * sig + (verb * revMix);
            sig = sig * 15.dbamp;
            sig = sig.tanh;
        });
    }

    prSetMags {
        arg cutoff=128;
        var vals = (0..256).collect{|i| 
            ( i > cutoff ).asInteger;
        };
        fx.set(\magnitudes, Ref( vals ));
    }

    mutate_ {
        arg m = true;
        mutate = m;
        if (mutate){ this.prMutateMags };
    }

    prMutateMags {
        arg cutoff=128, prob=0.5, tick=0.1;
        var vals = (0..256).collect{|i| 
            ( i > cutoff ).asInteger;
        };
        mutateMags = Routine.run{
            var x;
            KF.server.sync;
            inf.do{
                if(mutate){
                    if(prob.coin){
                        x = vals.removeAt(vals.size.rand);
                        vals.insert(vals.size.rand, x);
                    };
                    fx.set(\magnitudes, Ref( vals ));
                };
                tick.wait;
            };
        }.play;
    }

    free{
        super.free;
        mutateMags.stop;
    }
}
