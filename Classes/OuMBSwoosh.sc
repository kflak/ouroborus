OuMBSwoosh : OuMBDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.035, minAmp= -12, maxAmp=6, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBSwoosh;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buf = KF.buf[\swoosh].choose;

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, Pwhite(0.01, 0.1),
                \attack, Pkey(\dur),
                \release, Pkey(\dur),
                \startPos, Pfunc({|e| 
                    rrand(0, e.buf.numFrames - (e.dur * e.buf.sampleRate))
                }),
                \legato, 2,
                \rate, [0.5, 1].choose,
                \db, Pseg(
                    [-40, dt.linlin(0.0, 1.0, minAmp, maxAmp), -40],
                    [0.3, dt.linlin(0.0, 1.0, 1.0, 3.0)]
                ),
                \pan, Pwhite(-1.0, 1.0),
            );
        }    
    }

    initMBSwoosh{
        fx.filter(1, {|in|
            var lag = \lag.kr(0.1);
            var greyhole, greyholeMix = 0.3;
            var sig = in;
            var verb, verbMix = 0.3;
            verb = JPverbMono.ar(sig, size: 1.0, t60: 2);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            greyhole = Greyhole.ar(
                sig, 
                \delayTime.kr(0.3, lag),
                \damp.kr(0, lag),
                \size.kr(1, lag),
                \diff.kr(0.707, lag),
                \feedback.kr(0.3), 
                \modDepth.kr(0.1), 
                \modFreq.kr(2.0)
            );
            sig = (1 - greyholeMix) * sig + (greyhole * greyholeMix);
            sig = sig * \amp.kr(db.dbamp, lag);
            sig;
        })
    }

}
