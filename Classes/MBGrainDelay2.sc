MBGrainDelay2 : KFMBDeltaTrig {

    var <in;
    var <combMix = 0.2;
    var <verbMix = 0.5;
    var <combFreq = #[30, 50];
    var <rates = #[0.25,0.5, 1.0];
    var <inputLevel = 0;

    *new { arg db=0, speedlim=0.2, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBGrainDelay;
    }

    in_{|val| in = val; fx.set(\in, in); }
    combMix_{|val| combMix = val; fx.set(\combMix, combMix); }
    verbMix_{|val| verbMix = val; fx.set(\verbMix, verbMix); }
    combFreq_{|min=30, max=50|
        combFreq[0] = min;
        combFreq[1] = max;
        fx.set(\combFreqMin, combFreq[0], \combFreqMax, combFreq[1]);
    }
    rates_{|val| rates = val; fx.set(\rates, rates); }
    inputLevel_{|val| inputLevel = val; fx.set(\inputLevel, inputLevel); }

    initMBGrainDelay{
        fx[0] = {
            var input, sig;
            var grainsize = SinOsc.kr(0.1).range(0.005, 0.2);
            var trig = \trig.tr(0);
            var delay = 10;
            var buf = LocalBuf.new(KF.server.sampleRate * delay).clear;
            var pan = TRand.kr(-1.0, 1.0, trig);
            var drates = Drand(\rates.kr(rates), inf);
            var mixTrig = \mixTrig.tr(0);
            var release = \release.kr(3);
            var mix = EnvGen.kr(
                Env([0.2, 0.8, 0.2], [0.1, release]),
                mixTrig;
            );
            // var mix = \mix.kr(0.4);
            var grain, phasor, grainPos;
            var gain = 0;
            var currentPosInSeconds;
            var lag = 0.1;
            var verb, vMix = \verbMix.kr(verbMix, lag);
            var comb, cMix = \combMix.kr(combMix, lag);
            var combFreqMin = \combFreqMin.kr(30, lag);
            var combFreqMax = \combFreqMax.kr(50, lag);

            phasor = Phasor.ar(0, BufRateScale.kr(buf), 0, BufFrames.kr(buf));
            currentPosInSeconds = phasor / KF.server.sampleRate;
            grainPos = (currentPosInSeconds - TRand.kr(grainsize, delay, trig)) % delay;
            // input = \in.ar(in);
            input = In.ar(\in.kr(2));
            BufWr.ar(input, buf, phasor, loop: 1);
            input = input * \inputLevel.kr(inputLevel).dbamp;
            input = FreeVerb.ar(input, mix: 0.3, room: 1.0);
            grain = TGrains2.ar(
                2,
                trig,
                buf,
                rate: Demand.kr(trig, 0, drates),
                centerPos: grainPos,
                dur: grainsize,
                pan: pan
            );
            
            sig = grain;
            comb = CombC.ar(
                sig,
                0.2, 
                SinOsc.kr(0.1).range(combFreqMin, combFreqMax).reciprocal,
                2
            );
            comb = CombC.ar(
                comb,
                0.2,
                SinOsc.kr(0.14).range(combFreqMin, combFreqMax).reciprocal,
                1
            );
            comb = CombC.ar(
                comb,
                0.2,
                SinOsc.kr(
                    LFNoise2.kr(0.5).range(combFreqMin, combFreqMax)
                ).range(combFreqMin, combFreqMax).reciprocal,
                1
            );
            sig = (1 - cMix) * sig + (comb * cMix);

            verb = JPverbMono.ar(sig, size: 1.0, t60: 4);
            sig = (1 - vMix) * sig + (verb * vMix);
            // sig = sig * 10.dbamp;
            sig = sig * gain.dbamp;
            sig = sig.tanh;
            sig = Limiter.ar(sig, -2.dbamp, 0.05);
            sig = sig * \amp.kr(12.dbamp);
            sig = (1 - mix) * input + (sig * mix);
            sig;
        };
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            if(in.isNil){
                "No input specified".warn;
            }{
                var numGrains = dt.linlin(0.0, 1.0, 50, 200);
                var release = dt.linlin(0.0, 1.0, 1, 10);
                fx.set(\mixTrig, 1);
                fx.set(\release, release);
                numGrains.do{
                    fx.set(\trig, 1);
                    0.05.wait;
                }
            }
        }
    }
}
