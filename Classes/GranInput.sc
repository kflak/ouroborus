GranInput {
    var <in;
    var <maxDelayTime;
    var <db;
    var nodeProxy;
    var server;
    var inputmix = 0.3;
    var inputgain = 1;

    *new{|in=2, maxDelayTime=10, db=0|
        ^super.newCopyArgs(in, maxDelayTime, db).init;
    }

    init {
        server = Server.default;
        nodeProxy = NodeProxy.audio(server,2).play;
        nodeProxy.source = {
            var buf, sig, phasor, currentPosInSeconds, grainPos, input, delay=10, grainsize=0.2, mix, rates, posRates;
            buf = LocalBuf.new(server.sampleRate * delay).clear;
            phasor = Phasor.ar(0, BufRateScale.kr(buf) * \record.kr(1), 0, BufFrames.kr(buf));
            currentPosInSeconds = phasor / server.sampleRate;
            grainPos = (currentPosInSeconds - TRand.kr(grainsize, delay, Dust.kr(10))) % delay;
            input = In.ar(\in.kr(in) * \inputgain.kr(inputgain));
            BufWr.ar(input, buf, phasor);

            rates = 8.collect{ exprand(0.1, 2.0) } ++ 3.collect { exprand(2000, 8000)};
            rates = rates.sort;
            posRates = rates.size.collect({ exprand(0.01, 2.0)});
            sig = KFGran.ar(
                buf,
                rate: rates, 
                posRate: posRates, 
                posRateModFreq: rates.size.collect{ rrand(0.1, 2.0)},
                posRateModDepth: rates.size.collect{ rrand(0.1, 1.0)},
                jitter: 8.collect{ rrand(0.1, 0.5)},
                numChannels: 1
            );
            // sig = sig * rates.collect({|i| AmpCompA.kr(i) });
            sig = sig * 10.dbamp;
            sig = Splay.ar(sig);
            sig = FreeVerb.ar(sig, 0.7);
            sig = sig.scramble;
            mix = \mix.kr(inputmix, \lag.kr(1));
            sig = (1 - mix) * input + (sig * mix);
            sig = sig * \amp.kr(db.dbamp);
            sig = sig.tanh;
            sig;
        };
    }

    db_{|db|
        nodeProxy.set(\amp, db.dbamp);
    }

    inputgain_{|gaindb| 
        nodeProxy.set(\inputgain, gaindb.dbamp);
    }

    stopRecording {
        nodeProxy.set(\record, 0);
    }

    mixFadeTime {|fadeTime|
        nodeProxy.set(\lag, fadeTime);
    }

    inputmix_ {|mix|
        nodeProxy.set(\mix, mix);
    }


    free {|fadetime|
        nodeProxy.release(fadetime);
    } 
}
