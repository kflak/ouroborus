// Abstract superclass for all mbDeltaTrigs. Defines an
// mbDeltaTrig 
// OuMBDeltaTrig implements play and free functions.
OuMBDeltaTrig {

    var <db;
    var <speedlim;
    var <threshold;
    var <minAmp;
    var <maxAmp;
    var <fadeTime;
    var <fx;
    var <mbIDs;
    var <mbDeltaTrigs;
    var <>freeMBsImmediately = true;
    var <>freeMBsBufferTime = 1;
    var <server;
    var isPlaying;

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.newCopyArgs(db, speedlim, threshold, minAmp, maxAmp, fadeTime).init;
    }

    init {
        this.prCreateFx;
        mbIDs = KF.mb.copy;
        server = KF.server;
        this.prCreateMBDeltaTrigs;
    }

    set{|... args|
        if(args.size.mod(2) == 0, {
            args.pairsDo{|k, v|
                this.perform(k.asSetter, v);
            }
        })
    }

    // db_ {|val| fx.set(\amp, val.dbamp); db = val}
    db_ {|val| fx.vol = val.dbamp; db = val}
    speedlim_{|val| mbDeltaTrigs.do(_.speedlim_(val)); speedlim = val}
    threshold_{|val| mbDeltaTrigs.do(_.threshold_(val)); threshold = val}
    minAmp_{|val| mbDeltaTrigs.do(_.minAmp_(val)); minAmp = val}
    maxAmp_{|val| mbDeltaTrigs.do(_.maxAmp_(val)); maxAmp = val}
    fadeTime_{|val| fx.fadeTime = val; fadeTime = val}

    volUp {|inc=1| this.db_(db + inc); postf("Current dB: %\n", db)}
    volDown {|dec=1| this.db_(db - dec); postf("Current dB: %\n", db)}

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            "Triggered from superclass".warn;
            [id, dt, minAmp, maxAmp].postln;
        };
    }

    prCreateFx {
        fx = NodeProxy.audio(server, KF.numSpeakers);
        fx.play(fadeTime: fadeTime, vol: db.dbamp);
        // fx.set(\db, db);
        // fx.fadeTime = fadeTime;
    }

    prCreateMBDeltaTrigs{
        mbDeltaTrigs = mbIDs.collect{|id, idx|
            MBDeltaTrig.new(
                speedlim: speedlim, 
                threshold: threshold,
                minibeeID: id,
                minAmp: minAmp,
                maxAmp: maxAmp,
                function: this.mbDeltaTrigFunction;
            );
        }
    }

    play {|...args|
        var mbs;
        if(args.isEmpty){
            mbs = mbIDs;
        }{
            mbs = args;
        };

        mbs.do{|id| 
            var index = mbIDs.indexOf(id);
            mbDeltaTrigs[index].play;
        };
    }

    isPlaying{|mbID|
        var index = mbIDs.indexOf(mbID);
        ^mbDeltaTrigs[index].isPlaying;
    }

    freeMB {|mb|
        var index = mbIDs.indexOf(mb);
        mbDeltaTrigs[index].free;
        mbIDs.removeAt(index);
        if(mbIDs.isEmpty){
            this.free;
        }
    }

    free {|releaseTime|
        if(releaseTime.notNil){
            fadeTime = releaseTime;
        };
        fork{
            if (freeMBsImmediately){
                mbDeltaTrigs.do(_.free);
            }{
                SystemClock.sched(
                    max(fadeTime - freeMBsBufferTime, 0), 
                    {
                        mbDeltaTrigs.do(_.free);
                    }
                )
            };
            fx.release(fadeTime);
            fadeTime.wait;
            fx.clear;
        }
    }
}
