Ouroboros {
    classvar <packageRoot;
    classvar <audioDir;
    classvar <stereoDir;
    classvar <sketchDir;

    *initClass{
        packageRoot = Ouroboros.filenameSymbol.asString.dirname.dirname;
        audioDir = Ouroboros.packageRoot+/+"audio";
        stereoDir = Ouroboros.packageRoot+/+"stereo";
        sketchDir = Ouroboros.packageRoot+/+"sketches";
    }

    *new {
        ^super.new.init;
    }

    init { 
    }
}
