OuCocoon {
    var <db, <>fadeTime;
    var <nodeProxy;
    var <pattern;
    *new{|db = 0, fadeTime = 60|
        ^super.newCopyArgs(db, fadeTime).init;
    }

    db_{|val| db = val; nodeProxy.vol = db.dbamp}

    init {
        nodeProxy = NodeProxy.new;
        pattern = PatternProxy.new;
        pattern = Pbind(
            \instrument, \granulator,
            \buf, Prand(KF.buf[\xylophoneStretch], inf),
            \dur, Pfunc{|ev| ev.buf.duration * 4},
            \attack, Pkey(\dur),
            \release, Pkey(\dur),
            \rate, 1,
            \startPos, KF.server.sampleRate * 4,
            \rateModFreq, 0.11,
            \rateModDepth, 0.01,
            \posRate, 0.005,
            \posRateModFreq, 0.1,
            \posRateModDepth, 0.2,
            \pan, Pwhite(-1.0, 1.0),
            \db, Pwhite(-12, -6),
        );
        nodeProxy[0] = pattern;
        nodeProxy.play(fadeTime: fadeTime);
        nodeProxy.filter(1, {|in|
            var sig = in;
            var comb, combMix = 0.5;
            sig = BLowCut.ar(sig, 400);
            comb = [
                CombC.ar(sig[0], 1, rrand(0.2, 0.5), rrand(2, 3)),
                CombC.ar(sig[0] + sig[1], 1, rrand(0.2, 0.5), rrand(2, 3)),
                CombC.ar(sig[1], 1, rrand(0.2, 0.5), rrand(2, 3)),
            ];
            comb = Splay.ar(comb);
            sig = (1 - combMix) * sig + (comb * combMix);
            sig;
        })
    }

    free{|releaseTime|
        if(releaseTime.notNil){
            fadeTime = releaseTime;
        };
        nodeProxy.release(releaseTime);
    }
}
