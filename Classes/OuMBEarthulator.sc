OuMBEarthulator : OuMBDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.04, minAmp= -20, maxAmp=0, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBEarthulator;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|

            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\dancefloor]),
                // \endPos, Pfunc{|ev| ev.buf.duration },
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,  
                \rate, Prand([0.25, 1], inf),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 40),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    initMBEarthulator{
        fx.filter(1, {|in|
            var sig = in;
            sig = sig * 9.dbamp;
            sig = Compander.ar(sig, sig, -4.dbamp, slopeAbove: 1/8);
        });
    }
}
