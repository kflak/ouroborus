OuroborosTest1 : UnitTest {
	test_check_classname {
		var result = Ouroboros.new;
		this.assert(result.class == Ouroboros);
	}
}


OuroborosTester {
	*new {
		^super.new.init();
	}

	init {
		OuroborosTest1.run;
	}
}
