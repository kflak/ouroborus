OuMBWaterAliina : OuMBDeltaTrig {

    var <>pitchMB = 9;
    var <>verbMB = 11;
    var <>rateMB = 10;

    *new{
        arg db=0, speedlim=0.5, threshold=0.1, minAmp= -10, maxAmp=6, fadeTime=15;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initOuMBWaterAliina;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var dur = 1;
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, Prand(KF.buf[\mupsGiggles]),
                \dur, Pfunc{|ev| ev.buf.duration},
                \attack, 0.01,
                \release, 0.1,
                \rate, Pfunc{KF.mbData[rateMB].x.linlin(0.0, 1.0, 0.5, 2.0)},
                \tRateModFreq, 1,
                \tRateModDepth, 1,
                \posRate, 1,
                \posRateModFreq, 1,
                \posRateModDepth, 1,
                \pan, Pwhite(-1, 1),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
            fx[1] = Pbind(
                \instrument, \granulator,
                \buf, Prand(KF.buf[\runningWater], inf),
                \rate, Pexprand(0.5, 4),
                \dur, Pwhite(1, 3),
                \attack, Pkey(\dur) * 0.5,
                \release, Pkey(\dur) * 0.5,
                \tRateModFreq, Pwhite(1, 50),
                \tRateModDepth, 1,
                // \rateModFreq, 1,
                // \rateModDepth, 1,
                \posRate, Pwhite(1, 10),
                \posRateModFreq, Pwhite(1, 50),
                \posRateModDepth, 1,
                \pan, Pwhite(-1.0, 1.0, 1),
                \db, 5,
            )
        };
    }

    initOuMBWaterAliina {
        fx.filter(2, {|in|
            var sig, verb, verbMix, pitchshift, pitchMix=0.5;
            var mb9X = KF.mbData[pitchMB].xbus.kr;
            var mb10X = KF.mbData[verbMB].xbus.kr;
            sig = in;
            pitchshift = PitchShift.ar(sig, pitchRatio: mb9X.linexp(0, 1.0, 0.25, 8));
            sig = (1 - pitchMix) * sig + (pitchshift * pitchMix);
            verb = JPverbMono.ar(sig, size: 2, t60: 5);
            verbMix = mb10X;
            sig = (1 - verbMix) * sig + (verb * verbMix);
        });
    }
}
