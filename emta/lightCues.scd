(
var enttecPath = "/dev/serial/by-id/usb-ENTTEC_DMX_USB_PRO_EN149474-if00-port0";
if (File.exists(enttecPath)) {
    // all directions as seen from audience
    var pc = [0, 1, 3, 2]; // left front, left back, right front, right back
    var flood = [4, 5]; // left, right


    ~dmx = DMX.new;
    Tdef.defaultQuant = 0;
    ~enttec = EntTecDMXUSBPro.new(enttecPath);
    ~dmx.device = ~enttec;
    ~hasEnttec = true;

    ~waterLight = DMXCue.new;
    ~waterLight[pc[1]] = 0.5;

    ~generalLight = DMXCue.new;
    pc.do({|i| ~generalLight[i] = 0.5});
    flood.do({|i| ~generalLight[i] = 0.2});

    ~floodLeft = DMXCue.new;
    ~floodLeft[flood[0]] = 0.3;

    ~floodRight = DMXCue.new;
    ~floodRight[flood[1]] = 0.3;

    ~pcLeft = DMXCue.new;
    ~pcLeft[pc[0]] = 1.0;
    ~pcLeft[pc[1]] = 1.0;

    ~pcRight = DMXCue.new;
    ~pcRight[pc[2]] = 0.6;
    ~pcRight[pc[3]] = 0.6;

    ~pcLeftBack = DMXCue.new;
    ~pcLeftBack[pc[1]] = 1.0;

    ~pcRightBack = DMXCue.new;
    ~pcRightBack[pc[3]] = 0.5;

    ~pcLeftFront = DMXCue.new;
    ~pcLeftFront[pc[0]] = 1.0;

    ~pcRightFront = DMXCue.new;
    ~pcRightFront[pc[2]] = 0.5;

    ~camEdgeLight = DMXCue.new();
    ~camEdgeLight[pc[0]] = 0.5;
    ~camEdgeLight[pc[1]] = 0.5;
    ~camEdgeLight[pc[2]] = 0.5;
    ~camEdgeLight[pc[3]] = 0.5;

    Tdef(\camEdgeLight, {
        var fadeTime = 30;
        var addr = 0;
        inf.do{
            var q = DMXCue.new;
            var level = 0.8;

            if(addr > 2) {
                addr = 0;
            };
            q[addr] = level;
            q[addr+1] = level;

            ~dmx.fade(q, fadeTime);

            addr = addr + 2;

            fadeTime.wait;
        }
    });

    Tdef(\interactiveSideLights, {
        var fadeTime = 0.05;
        var min = 0.5;
        var max = 2.0;
        inf.do{
            var q = DMXCue.new;

            q[pc[0]] = KF.mbData[9].delta.linlin(0.0, 1.0, min, max);
            q[pc[1]] = KF.mbData[10].delta.linlin(0.0, 1.0, min, max);
            q[pc[2]] = KF.mbData[11].delta.linlin(0.0, 1.0, min, max);
            q[pc[3]] = KF.mbData[12].delta.linlin(0.0, 1.0, min, max);

            ~dmx.fade(q, fadeTime);
            fadeTime.wait;
        }
    });

    Tdef(\sineSideLightsRight, {
        var envelopeTime = 4;
        var waitTime = 0.1;
        var base = Pseg([0.0, 1.0, 0.0], [envelopeTime, envelopeTime]/2, \sine, repeats: inf).asStream;
        var modulationRate = 20;
        var modulator = Pseg([0.0, 1.0, 0.0], [20, 20]/2, repeats: inf).asStream;
        var val = base * modulator;
        inf.do{
            var q = DMXCue.new;
            q[pc[2]] = val.next;
            q[pc[3]] = val.next;
            ~dmx.fade(q, waitTime);
            waitTime.wait;
        }
    });

    Tdef(\sineSideLightsLeft, {
        var envelopeTime = 4;
        var waitTime = 0.1;
        var base = Pseg([0.0, 0.1, 0.0], [envelopeTime, envelopeTime]/2, \sine, repeats: inf).asStream;
        var modulationRate = 20;
        var modulator = Pseg([0.0, 1.0, 0.0], [20, 20]/2, repeats: inf).asStream;
        var val = base * modulator;
        inf.do{
            var q = DMXCue.new;
            q[pc[0]] = val.next;
            q[pc[1]] = val.next;
            ~dmx.fade(q, waitTime);
            waitTime.wait;
        }
    });

    Tdef(\sineSideLightsFloodLeft, {
        var envelopeTime = 16;
        var waitTime = 0.1;
        var val = Pseg([0.0, 1.0, 0.0], [envelopeTime, envelopeTime]/2, \sine, repeats: inf).asStream;
        inf.do{
            var q = DMXCue.new;
            q[flood[0]] = val.next * 0.1;
            ~dmx.fade(q, waitTime);
            waitTime.wait;
        }
    });


    Tdef(\flicker, {
        var fadeTime = 0.1;
        var min = 0.3;
        var max = 0.5;
        var numFixtures = (pc++flood).size;
        var values = [max] ++ (min ! (numFixtures - 1));
        inf.do{
            var q = DMXCue.new;
            var v = values.scramble;
            q[pc[0]] = v[0];
            q[pc[1]] = v[1];
            q[pc[2]] = v[2];
            q[pc[3]] = v[3];
            q[flood[0]] = v[4] * 0.1;
            q[flood[1]] = v[5] * 0.1;
            ~dmx.fade(q, fadeTime);
            fadeTime.wait;
        }
    });

    ~slitScanLight = DMXCue.new;
    ~slitScanLight[pc[1]] = 0.4;

    ~granTalkLight = DMXCue.new;
    ~granTalkLight[flood[0]] = 0.4;
    ~granTalkLight[flood[1]] = 0.4;

    ~dmx.blackout;
    "light cues loaded".postln;
}{
    "no enttec interface connected".warn;
})
