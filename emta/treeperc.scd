(
"treeperc".postln;
Ndef(\treeperc);
Pdef(\treeperc,
    Pbind(
        \instrument, \playbuf,
        \dur, 1/8,
        \buf, Prand(KF.buf[\treeperc], inf),
        \release, Pwhite(0.02, 0.3),
        \db, Pwhite(-13, -2),
        \distortion, Pwhite(30, 60),
        \legato, 0.2,
        \pan, Pwhite(-1.0, 1.0),
        \duck, Pfunc{
            Ndef(\impulses).set(\retrig, 1);
            // Ndef(\fbSystem).set(\retrig, 1);
        },
    )
);
Ndef(\treeperc, Pdef(\treeperc)).quant_(1).play;
Ndef(\treeperc).filter(2, {|in| 
    var verb, verbMix, sig;
    var delay, delayMix;
    var deltaA = [
        KF.mbData[9].deltabus.kr,
        KF.mbData[11].deltabus.kr
    ].sum;
    var deltaB = [
        KF.mbData[10].deltabus.kr,
        KF.mbData[12].deltabus.kr
    ].sum;
    sig = in;
    sig = BHiPass.ar(sig, 200);
    delay = CombC.ar(sig, 0.2, 1/32, 2);
    delayMix = deltaA.lag2ud(0, 2).clip(0, 1);
    sig = (1 - delayMix) * sig + (delay * delayMix);
    verb = JPverbMono.ar(sig, size: 2, t60: 5);
    verbMix = deltaB.lag2ud(0, 2).clip(0, 1);
    sig = (1 - verbMix) * sig + (verb * verbMix);
});
)
