(
~mbCeramics = KF.mb.collect({|i|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.05,
        minibeeID: i,
        minAmp: -70,
        maxAmp: 0,
        function: {|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \granulator,
                \buf, Prand(KF.buf[\ceramics]),
                \dur, Pfunc{|ev| min(2, ev.buf.duration)},
                \attack, 0.01,
                \release, Pkey(\dur) / 2,
                \rate, Prand([0.5, 1, 2]),
                \tRateModFreq, Pwhite(10, 100),
                \tRateModDepth, 0.1,
                \rateModFreq, Pwhite(10, 100),
                \rateModDepth, 0.01,
                \posRate, Pexprand(0.1, 2),
                \posRateModFreq, Pwhite(10, 100),
                \posRateModDepth, 1,
                \pan, Pwhite(-1.0, 1.0),
                \db, 0,
                \out, 0,
            ).play;
        }
    ).play;
});
)
// ~mbCeramics.do(_.free);
