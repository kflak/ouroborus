---
geometry: a4paper
title: Structure of the piece
author: Kenneth Flak
titlepage: false
disable-header-and-footer: false
date: 2023-12-27
urlcolor: magenta
---

## 1. Driftwood

Movement:

- Disjointed
- Minimal
- Nothing really works
- Moves into copying
- vector line
- Ends with triple hands on the floor

Sound:

- tweeps
- random motors, sparsely distributed
- TODO: crackling, breaking sounds from the sensors
- TODO: pre-verbal sounds
- sand
- walkGravel

## 2. Dysfunctional relationships

Preparing the space for the rain dance.

Movement:
2.1 Line vector
2.2 Lifting
2.3 Külli mic + KR encounter. K block Klli with boxing pad? R uses mics
to bring us into:
2.4 Külli mic - Stuckness

Sound:
2.1
2.2
2.3
2.4

## 3. Minimal Cactus Band

Movement:

- everybody stuck, squatting with mics
- occasional twitches

Sound:

- Mics, pre-verbal noises, breaths, twitches
- grainDelay2?
- TODO: Chaotic oscillators sensors
- bassDrone
- aliina granular song CC
- crescendos to a big texture

## 4. Slapping

Sound:

- micSlap stops big crescendo
- TODO: trigger words as well as cello
- What to do with the other 2 mics?

## 5. Cocoon

Clean cut

Movement:

- Circularity
- Spine waves
- slow motion

Sound:

- Arctic, beautiful, Alva Noto style
- Breathing?
- Elements of triple lullaby?
- xylophoneStretch reversed

## 6. Rain Dance

Movement:

- phrase

Sound:

- euclidianRhytms
- motoros
- knives (fbSystem)
- impulses

## 7. Holding On Trio

Movement:

- Holding each other's hands, leaning out, pushing in, ending up flat on
  the floor.

Sound:

- Triple lullaby

Light:

- Motor rotating in front of a floor lamp, creating shadows.
