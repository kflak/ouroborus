(
"Starting granCloud".postln;
Ndef(\granCloud).clear;
Ndef(\granCloud).play;
Ndef(\granCloud).fadeTime = 50;
Ndef(\granCloud, {
    var tRate = SinOsc.kr(0.1).range(20, 100);
    var trig = Impulse.kr(tRate);
    var bufnum = KF.buf[\ceramics][0];
    var grainSize;
    var verb, verbMix = LFNoise2.kr(0.2).range(0.0, 0.3);
    // var grain;
    var sig;
    var mod, modMix;
    var rates = Drand([0.125, 0.25, 0.5, 1, 2, 4], inf);
    var density, ampScale;
    var chopper, chopperMix = LFNoise2.kr(0.2).range(0.0, 1.0);

    grainSize = SinOsc.kr(
        SinOsc.kr(0.2).range(0.02, 0.5)
    ).range(5/tRate, 1);

    density = tRate * grainSize;

    ampScale = density.reciprocal;

    sig = TGrains2.ar(
        KF.numSpeakers,
        trig,
        bufnum,
        rate: Demand.kr(trig, 0, rates),
        centerPos: Phasor.ar(0, BufRateScale.kr(bufnum), 0, BufFrames.kr(bufnum)),
        dur: grainSize,
        pan: TRand.kr(-1.0, 1.0, trig),
        amp: 0.1 * ampScale,
    );


    // ring modulation
    // modMix = KF.mbData[1].xbus.kr.linlin(0.0, 1.0, 0.0, 0.8);
    modMix = LFNoise2.kr(0.3).range(0.0, 0.8);
    mod = sig * SinOsc.ar(LFNoise2.kr(0.2).exprange(50, 1600));
    sig = (1 - modMix) * sig + (mod * modMix);

    // slow amplitude modulation
    // sig = sig * SinOsc.kr(
    //     SinOsc.kr(
    //         SinOsc.kr(0.12)
    //     ).range(0.01, 0.3)
    // ).range(0.6, 1.0);

    chopper = LFPulse.kr(
        SinOsc.ar(0.1).range(0.5, 32)
    ).range(
        SinOsc.kr(0.1).range(0.2, 1.0), 1.0
    ).lag(0.01);
    // sig = (1 - chopperMix) * sig + (chopper * chopperMix);
    chopper = 1 - (chopper * chopperMix);
    sig = sig * chopper;

    verb = JPverbMono.ar(sig, size: 2, t60: SinOsc.kr(0.2).range(0.5, 3));
    sig = (1 - verbMix) * sig + (verb * verbMix);

    sig = BLowPass.ar(sig, LFNoise2.kr(0.5).range(150, 20000));
    // sig = BHiCut.ar(sig, KF.mbData[1].xbus.kr.linexp(0.0, 1.0, 150, 20000));
    sig = sig * -6.dbamp;
    sig = sig * \amp.kr(0.dbamp);
    sig;
});
)


// (
// Ndef(\sub).release(4);
// Ndef(\granCloud).release(4);
// )

// (
// Ndef(\sub).clear;
// Ndef(\granCloud).clear;
// Tdef(\granCloud).stop;
// )

