(
"Starting crackle".postln;
Pdef(\crackle,
    Pbind(
        \instrument, \crackle,
        \attack, 0,
        \release, 1,
        \dur, Pwrand([
            Pwhite(0.1, 2, 1),
            Rest(0.25),
            Rest(0.5),
            Rest(1),
        ], [0.8, 0.2, 0.1, 0.1].normalizeSum, inf),
        \freq, Pseg([200, 2000, 2000], [60, inf]),
        \db, Pwhite(-40, -30) + Pseg([0, 20, 20], [60, inf]),
        \pan, Pwhite(-1.0, 1.0),
    );
);
Ndef(\crackle, Pdef(\crackle)).play(fadeTime: 20);
Ndef(\crackle).filter(1, {|in|
    var verb, revMix, sig;
    var delay, delayMix=0.1;
    sig = in * 30.dbamp;
    sig = sig.tanh;
    // TODO: filter to mb.x
    sig = BLowCut.ar(sig, 900);
    delay = CombC.ar(sig, 0.3, delaytime: LFNoise2.kr(1).range(0.1, 0.3), decaytime: 3);
    sig = (1 - delayMix) * sig + (delay * delayMix);
    verb = JPverbMono.ar(in, size: 1, t60: 2);
    revMix = SinOsc.kr(0.2).range(0.1, 0.4);
    sig = (1 - revMix) * sig + (verb * revMix);
    sig = sig * -6.dbamp;
});
)

