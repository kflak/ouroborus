KF.start;

(
Ndef(\fbSystem, {
    var sig, numCombFilters=8, modFreq, verb, mix;
    var delayTime, minDelayTime, maxDelayTime;

    minDelayTime = 2000.reciprocal;
    maxDelayTime = 2;

    delayTime = numCombFilters.collect({
        rrand(minDelayTime, maxDelayTime)}
    );

    modFreq = numCombFilters.collect({
        rrand(0.05, 0.1);
    });

    sig = numCombFilters.collect({ 
        Pulse.ar(SinOsc.kr(0.1).exprange(rrand(20, 40), rrand(200, 880)));
    });

    sig = numCombFilters.collect({|i|
        CombC.ar(sig[i], maxDelayTime, delayTime[i] * SinOsc.kr(modFreq[i]).exprange(1, 4));
    });

    sig = Splay.ar(sig);

    sig = CombC.ar(sig, maxDelayTime, SinOsc.kr(0.1).exprange(20, 40).reciprocal);
    sig = CombC.ar(sig, maxDelayTime, SinOsc.kr(0.2).exprange(30, 60).reciprocal);
    sig = CombC.ar(sig, maxDelayTime, SinOsc.kr(0.3).exprange(20, 200).reciprocal);
    sig = CombC.ar(sig, maxDelayTime, SinOsc.kr(0.4).exprange(20, 400).reciprocal);

    sig = sig * SinOsc.ar(SinOsc.kr(SinOsc.kr(0.1).range(0.01, 0.3)).exprange(20, 200));
    sig = sig * SinOsc.ar(SinOsc.kr(SinOsc.kr(0.1).range(0.03, 0.5)).exprange(40, 600));
    sig = sig * SinOsc.ar(SinOsc.kr(SinOsc.kr(0.1).range(0.03, 0.5)).exprange(40, 1200));

    sig = MoogVCF.ar(sig, SinOsc.kr(0.1).exprange(50, 2000), 1.0);

    sig = sig.tanh;

    verb = JPverb.ar(sig);
    mix = 0.4;
    sig = (1 - mix) * sig + (verb * mix);

    sig = sig * -10.dbamp;

}).play;
)

Ndef(\fbSystem).clear;
Ndef(\fbSystem).release(10);
